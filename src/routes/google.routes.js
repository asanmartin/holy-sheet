const express = require('express');
//const dotenv = require('dotenv').config();
const routes = express.Router()

const {
    getInfo,
    getById,
    addNewRegister,
    newForm,
    getHome,
    test
} = require('../controllers/google.controller');

routes.get('/correo/:correo', getInfo);
routes.get('/correo/:correo/:id', getById);
routes.get('/form', newForm);
routes.post('/form', addNewRegister);
routes.get('/home', getHome)
routes.get('/test', test)

module.exports = routes;