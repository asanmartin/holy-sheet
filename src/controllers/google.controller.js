const dotenv = require('dotenv').config();
const { GoogleSpreadsheet } = require('google-spreadsheet')
const fs = require('fs');

const doc = new GoogleSpreadsheet(process.env.RESPONSES_SHEET_ID);
const CREDENTIALS = JSON.parse(fs.readFileSync('./src/json/keys.json'));

async function accessSpreadSheet(page){
    await doc.useServiceAccountAuth({
        client_email: CREDENTIALS.client_email,
        private_key: CREDENTIALS.private_key
    });

    // load the documents info
    await doc.loadInfo();

    // Index of the sheet
    return doc.sheetsByTitle[page]; // or use doc.sheetsById[id] or doc.sheetsByTitle[title]
}
const test = async (req, res) => {
    res.render('test')
}

const getHome = async (req, res) => {
    res.render('home', {})
}

const getInfo = async (req, res) => {
    let sheet = await accessSpreadSheet(req.params.correo);

    let rows = await sheet.getRows();
    //console.log(rows);

    res.render('tabla', {rows});
}

const getById = async (req, res) => {
    let sheet = await accessSpreadSheet('search');

    await sheet.loadCells('A1:J2'); // loads a range of cells

    const searchcode = sheet.getCellByA1('H2');
    const searchsheet = sheet.getCellByA1('J2');
    //console.log(busqueda);
    //console.log(req.params.id);

    searchcode.value = req.params.id;
    searchsheet.value = req.params.correo;

    await sheet.saveUpdatedCells();
    
    let opt = {
        offset: 0,
        limit: 1
    };
    let rows = await sheet.getRows(opt);
    //console.log(rows);

    res.render('index', {rows});
}

const newForm = (req, res) => {
    res.render('form', {});
}

const addNewRegister = async (req, res) => {
    const reg = req.body;
    // console.log(reg);
    let sheet = await accessSpreadSheet(reg.correo);
    
    await sheet.addRow(reg);
    
    //res.redirect('/api/correo/form');
    res.redirect('back');
}


// // insertar un nuevo registro
// routes.post('/:correo', async (req, res) => {
    
//     //console.log(req.body);
//     let sheet = await accessSpreadSheet(req.params.correo);
    
//     await sheet.addRow(req.body);
    
//     res.send('successfully inserted, prro');
// })

// // actualizar un registro determinado
// routes.put('/:correo/:id', async (req, res) => {
//     let sheet = await accessSpreadSheet('search');

//     await sheet.loadCells('A1:J2'); // loads a range of cells

//     const searchcode = sheet.getCellByA1('H2');
//     const searchsheet = sheet.getCellByA1('J2');
//     //console.log(busqueda);
//     //console.log(req.params.id);

//     searchcode.value = req.params.id;
//     searchsheet.value = req.params.correo;

//     await sheet.saveUpdatedCells(); //actualizo
//     const searchrow = sheet.getCellByA1('I2') -2; //obtengo la fila -2 por el indice corrido

//     console.log(searchrow);
// })

module.exports = {
    getInfo: getInfo,
    getById: getById,
    newForm: newForm,
    addNewRegister: addNewRegister,
    getHome: getHome,
    test: test
}