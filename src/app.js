const express = require('express');
const routes = require('./routes/google.routes');
const cors = require('cors');
const path = require('path');
const logger = require('morgan');
//const dotenv = require('dotenv').config();

// Create a new document
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// middlewares --------------
app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Bootstrap 4 y librerías necesarias
app.use(express.static(path.join(__dirname, 'public')));
// app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
// app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
// app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));
// app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));

// routes ---------------
app.get('/', (req, res)=>{
    res.send('Welcome to any api')
});
app.use('/api', routes);

module.exports = app;