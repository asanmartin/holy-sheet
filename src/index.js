const app = require('./app');

app.set('port', process.env.PORT || 3000);

// server running -------
app.listen(app.get('port'), () => {
    console.log('server running on port', app.get('port'))
});